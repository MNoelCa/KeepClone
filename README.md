//PASOS PARA EJECUTAR LA APP

// Base de Datos
No es necesario iniciar una base de datos, está deployada.
tipo: PostgreSQL

// Back end
1. Posicionarse en la carpeta /api
2. Abrir una consola y ejecutar el comando "npm install"
3. Ejecutar el comando "npm start" para iniciar el backend.

// Front end
1. Posicionarse en la carpeta /client/KeepClone
2. Abrir una consola y ejecutar el comando "npm install"
3. Ejcutar el comando "npm run dev" para iniciar el frontend
4. Abrir el navegador en la ruta que indique la app (p.ej. "http://127.0.0.1:5173/")
