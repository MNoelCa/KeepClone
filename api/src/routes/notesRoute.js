const { Router } = require('express');
const router = Router();
const { Note } = require('../db.js');


router.get('/', async (req,res) => {
    try {
        const notes = await Note.findAll({
            where:{
                isArchived:false
            },
            order: [
                ["createdAt", 'DESC']
            ]
        });
        res.send(notes);
    } catch(err) {
        res.status(500).send({error: err.message});
    }
});

router.post('/', async (req,res) => {
    const {title, content, isFixed, isArchived} = req.body;
    if (title || content) {
        try {
            const newNote = await Note.create({
                title: title,
                content: content,
                isFixed: isFixed,
                isArchived: isArchived
            })
            res.status(201).send(newNote)
        } catch(err) {
            res.status(500).send({error: err.message});
        }
    } else {
        res.status(400).send('Tu nota está vacía.')
    }
});

router.delete('/:id', async (req,res) => {
    try {
        const { id } = req.params;
        const deleteNote = await Note.findOne({ where:{id:id} });
        await deleteNote.destroy();
        res
          .status(200)
          .send({ message: "The Note was deleted successfully" });
    } catch (err) {
        res.status(500).send({
          message: "The Note can´t be deleted",
        });
    }
});

router.put('/:id', async (req,res) => {
    const { id } = req.params;
    const { title, content, isFixed, isArchived} = req.body;
    if (title || content || isFixed || isArchived) {
        try {
            const note = await Note.findByPk(id)
            note.title = title ? title : note.title;
            note.content = content ? content : note.content;
            (isFixed !== undefined) && (note.isFixed = isFixed);
            (isArchived !== undefined) && (note.isArchived = isArchived) 
            await note.save()
            res.send(note)
        } catch(err) {
            res.status(500).send({error: err.message});
        }
    } else {
        res.status(400).send('Tu nota está vacía.')
    }
});

router.get('/archived', async (req,res) => {
    try {
        const archived = await Note.findAll({
            where:{
                isArchived:true
            },
            order: [
                ["createdAt", 'DESC']
            ]
        });
        res.send(archived);
    } catch(err) {
        res.status(500).send({error: err.message});
    }
});


/* router.put('/:id/archived', async (req,res) => {
    const { id } = req.params;
    console.log(id)
    const { isArchived } = req.body;
    try {
        const note = await Note.findByPk(id)
        (isArchived !== undefined) && (note.isArchived = isArchived) 
        await note.save()
        res.send(note)
    } catch(err) {
        res.status(500).send({error: err.message});
    }
}); */




module.exports = router;