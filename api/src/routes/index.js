const { Router } = require('express')
const notes = require('./notesRoute.js')
const router = Router();

router.use('/notes', notes)

module.exports = router;
